import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import AUIBreadcrumbs from '../src/AUIBreadcrumbs';

describe('AUIBreadcrumbs', () => {
    it('should render the correct AUI markup', () => {
        expect(shallow(<AUIBreadcrumbs />).html()).to.equal('<ol class="aui-nav aui-nav-breadcrumbs"></ol>');
    });
});
