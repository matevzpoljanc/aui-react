import React from 'react';
import { shallow } from 'enzyme';
import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import AUITooltip from '../src/AUITooltip';

chai.use(chaiEnzyme());

describe('AUITooltip', () => {
    it('should render the correct markup ', () => {
        const wrapper = shallow(<AUITooltip gravity='n'>Test</AUITooltip>);
        expect(wrapper.html()).to.equal(`<div role="tooltip" class="tipsy tipsy-n"><div class="tipsy-arrow tipsy-arrow-n"></div><div class="tipsy-inner">Test</div></div>`);
    });
});
