[![Coverage Status](https://coveralls.io/repos/bitbucket/atlassian/aui-react/badge.svg?branch=master)](https://coveralls.io/bitbucket/atlassian/aui-react?branch=master)

Javascript behaviour and styles are provided by AUI. You should include the relevant AUI files based on [the AUI documentation](https://docs.atlassian.com/aui/latest/).

# Usage

## ES6 (recommended)

```js
import React, { Component } from 'react';

// You could also use "import AUI from 'aui-react';", but this would increase your bundle size in most cases.
// By consuming the component directly, you ensure only the component and its dependencies are included.
import AUIFontIcon from 'aui-react/lib/AUIFontIcon';

export default class MyApproveIcon extends Component {
    return <AUIFontIcon type="approve" />;
}
```

## CommonJS/Node

```js
var AUI = require('aui-react');

var MyApproveIcon = React.createClass({
    return <AUI.FontIcon type="approve" />;
});

module.exports = MyApproveIcon;
```

## Global

```html
<div id="container"></div>
<script src="node_modules/aui-react/dist/aui-react.js"></script>
<script type="text/babel">
    ReactDOM.render(<AUI.FontIcon type="approve" />, document.getElementById('container'));
</script>
```

# Development Guide

Contributions are encouraged!

## UI testing

Option 1 (recommended). 
> * In aui-react, run `npm link .` and `npm run watch`.
> * Link aui-react to your application using `npm link aui-react`.

Option 2.
> * Each time you want to test the changes, use `npm install path/to/aui-react` to install them to your application.

## Unit testing

    $ npm test
    
## Running linter

    $ npm run lint

## Checking test coverage

    $ npm run coverage

## Releasing

Releases can be prepared by anyone with access to the code.

Simply bump the version number following [semantic versioning](http://semver.org/) and make a commit to a branch

All you need to do is push the commit up and make a pull request, one of the admins will merge it and publish a release.

# Components

## Avatars

```js
<AUI.Avatar
    src="{string}"
    [size="xsmall|small|medium|large|xlarge|xxlarge|xxxlarge"]
    [isProject={boolean=false}]
/>
```

![AUI.Avatar](https://bytebucket.org/atlassian/aui-react/raw/master/docs/screenshots/avatar.jpg)

## Application header

```js
<AUI.ApplicationHeader
    product="aui|bamboo|bitbucket|confluence|crowd|fecru|hipchat|jira|stash"
    [primaryContent={Component}]
    [secondaryContent={Component}]
/>
```

![AUI.ApplicationHeader](https://bytebucket.org/atlassian/aui-react/raw/master/docs/screenshots/application-header.png)

## Buttons

```js
<AUI.Button
    [type="link|primary|subtle"]
    [icon={string}]
/>
```

![AUI.Button](https://bytebucket.org/atlassian/aui-react/raw/master/docs/screenshots/buttons.png)

## Icons

```js
<AUI.Icon
    icon={string}
/>
```

![AUI.Icons](https://bytebucket.org/atlassian/aui-react/raw/master/docs/screenshots/icons.png)

## Tables

```js
<AUI.table>
    <thead>
        <tr>
            <th>...</th>
            ...
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>...</td>
            ...
        </tr>
    </tbody>
</AUI.table>
```

![AUI.Icons](https://bytebucket.org/atlassian/aui-react/raw/master/docs/screenshots/tables.png)

## Tabs

```js
<AUI.Tabs>
    <AUI.Tab label="First Tab" id="first-tab">
        <p>Content of the <strong>first</strong> tab</p>
    </AUI.Tab>

    <AUI.Tab label={(<span>Second Tab <AUI.Badge text="123" /></span>)} id="second-tab">
        You can pass components as a tab label and to the tab content.
        It simply <AUI.Lozenge type="success">works</AUI.Lozenge>
    </AUI.Tab>

    <AUI.Tab label="Last tab" id="last-tab" isActive>
        <p>The last tab will be an active tab</p>
    </AUI.Tab>
</AUI.Tabs>
```

![AUI.Tabs](https://bytebucket.org/atlassian/aui-react/raw/master/docs/screenshots/tabs.png)
