import React, { Fragment } from 'react';
import { render } from 'react-dom';
import {
    ApplicationHeader,
    Avatar,
    Badge,
    Banner,
    Button,
    ButtonGroup,
    Icon,
    Lozenge,
    Nav,
    NavGroup,
    NavHeading,
    NavItem,
    PageContent,
    PageHeader,
    Table,
    Tab,
    Tabs
} from '../src/index';

const pageHeader = (<PageHeader headerText="AUI (React) Documentation"/>);

const nav = (
    <NavGroup>
        <NavHeading>Components</NavHeading>
        <Nav>
            <NavItem href="#avatars">Avatars</NavItem>
            <NavItem href="#badges">Badges</NavItem>
            <NavItem href="#banners">Banners</NavItem>
            <NavItem href="#buttons">Buttons</NavItem>
            <NavItem href="#button-group">Button group</NavItem>
            <NavItem href="#icons">Icons</NavItem>
            <NavItem href="#lozenges">Lozenges</NavItem>
            <NavItem href="#tables">Tables</NavItem>
            <NavItem href="#tabs">Tabs</NavItem>
        </Nav>
        <NavHeading>Page layouts</NavHeading>
        <Nav>
            <NavItem href="#application-header">Application header</NavItem>
        </Nav>
    </NavGroup>
);

const globalIcons = ["add", "add-comment", "add-small", "approve", "appswitcher", "arrows-down", "arrows-left", "arrows-right", "arrows-up", "attachment", "attachment-small", "autocomplete-date", "back-page", "blogroll", "bp-decisions", "bp-default", "bp-files", "bp-requirements", "bp-howto", "bp-jira", "bp-meeting", "bp-retrospective", "bp-sharedlinks", "bp-troubleshooting", "build", "calendar", "close-dialog", "collapsed", "comment", "configure", "confluence", "copy-clipboard", "custom-bullet", "delete", "deploy", "details", "doc", "down", "drag-vertical", "edit", "edit-small", "email", "error", "expanded", "file-code", "file-doc", "file-java", "file-pdf", "file-ppt", "file-txt", "file-wav", "file-xls", "file-zip", "flag", "focus", "group", "handle-horizontal", "help", "hipchat", "homepage", "image", "image-extrasmall", "image-small", "info", "like", "like-small", "weblink", "link", "list-add", "list-remove", "locked", "locked-small", "macro-code", "macro-default", "macro-gallery", "macro-status", "more", "nav-children", "page-blank", "page-blogpost", "page-default", "page-template", "pages", "quote", "redo", "remove", "remove-label", "review", "rss", "search", "search-small", "share", "sidebar-link", "sourcetree", "space-default", "space-personal", "star", "success", "table-bg", "table-col-left", "table-col-remove", "table-col-right", "table-copy-row", "table-cut-row", "table-header-column", "table-header-row", "table-merge", "table-no-bg", "table-paste-row", "table-remove", "table-row-down", "table-row-remove", "table-row-up", "table-split", "teamcals", "time", "undo", "unfocus", "unlocked", "unstar", "unwatch", "up", "user", "user-status", "view", "view-card", "view-list", "view-table", "warning", "watch", "workbox", "workbox-empty", "configure-columns", "export", "export-list", "file-image", "admin-fusion", "admin-jira-fields", "admin-issue", "admin-notifications", "admin-roles", "admin-jira-screens", "pause", "priority-highest", "priority-high", "priority-medium", "priority-low", "priority-lowest", "refresh-small", "share-list", "switch-small", "version", "workflow", "admin-jira-settings", "component", "reopen", "roadmap", "deploy-success", "deploy-fail", "file-generic", "arrow-down", "arrow-up", "file-video", "blogroll-large", "email-large", "layout-1col-large", "layout-2col-large", "layout-2col-left-large", "layout-2col-right-large", "layout-3col-center-large", "layout-3col-large", "nav-children-large", "pages-large", "sidebar-link-large", "teamcals-large", "user-large", "jira-issues"];
const devToolsIcons = ["devtools-arrow-left", "devtools-arrow-right", "devtools-branch", "devtools-branch-small", "devtools-browse-up", "devtools-checkout", "devtools-clone", "devtools-commit", "devtools-compare", "devtools-file", "devtools-file-binary", "devtools-file-commented", "devtools-folder-closed", "devtools-folder-open", "devtools-fork", "devtools-pull-request", "devtools-repository", "devtools-repository-forked", "devtools-repository-locked", "devtools-side-diff", "devtools-submodule", "devtools-tag", "devtools-tag-small", "devtools-task-cancelled", "devtools-task-disabled", "devtools-task-in-progress", "bitbucket"];
const editorIcons = ["editor-align-center", "editor-align-left", "editor-align-right", "editor-bold", "editor-color", "editor-emoticon", "editor-help", "editor-hr", "editor-indent", "editor-italic", "editor-layout", "editor-list-bullet", "editor-list-number", "editor-macro-toc", "editor-mention", "editor-outdent", "editor-styles", "editor-symbol", "editor-table", "editor-task", "editor-underline"];
const jiraIcons = ["jira", "jira-completed-task", "jira-test-session"];

render((
    <div id="page">
        <ApplicationHeader logo="aui"/>
        <PageContent
            pageHeader={pageHeader}
            nav={nav}
        >
            <h2 id="avatars">Avatars</h2>
            <div className="aui-flatpack-example">
                <Avatar size="xsmall" src="//docs.atlassian.com/aui/latest/docs/images/avatar-16.png"/>
                <Avatar size="small" src="//docs.atlassian.com/aui/latest/docs/images/avatar-24.png"/>
                <Avatar size="medium" src="//docs.atlassian.com/aui/latest/docs/images/avatar-32.png"/>
                <Avatar size="large" src="//docs.atlassian.com/aui/latest/docs/images/avatar-48.png"/>
                <Avatar size="xxlarge" src="//docs.atlassian.com/aui/latest/docs/images/avatar-96.png"/>
                <Avatar size="small" src="//docs.atlassian.com/aui/latest/docs/images/project-24.png" isProject/>
                <Avatar size="medium" src="//docs.atlassian.com/aui/latest/docs/images/project-32.png" isProject/>
                <Avatar size="large" src="//docs.atlassian.com/aui/latest/docs/images/project-48.png" isProject/>
                <Avatar size="xlarge" src="//docs.atlassian.com/aui/latest/docs/images/project-64.png" isProject/>
                <Avatar size="xxxlarge" src="//docs.atlassian.com/aui/latest/docs/images/project-128.png" isProject/>
            </div>

            <h2 id="badges">Badges</h2>
            <div className="aui-flatpack-example">
                <Badge text="1"/>
                <Badge text="2"/>
                <Badge text="3"/>
                <Badge text="4"/>
                <Badge text="5"/>
                <Badge text="6"/>
                <Badge text="7"/>
            </div>

            <h2 id="banners">Banners</h2>
            <div className="aui-flatpack-example" style={{height: 80}}>
                <Banner>Your <strong>license has expired!</strong> There are two days left to <a>renew your
                    license</a>.</Banner>
                <ApplicationHeader logo="aui"/>
            </div>

            <h2 id="buttons">Buttons</h2>
            <div className="aui-flatpack-example">
                <Button>Button</Button>
                <Button type="primary">Primary Button</Button>
                <Button type="link">Link Button</Button>
                <Button icon="view">Icon Button</Button>
                <Button disabled>Disabled Button</Button>
                <Button type="subtle" icon="configure">Subtle Button</Button>
            </div>

            <h2 id="button-group">Button group</h2>
            <div className="aui-flatpack-example">
                <ButtonGroup>
                    <Button>Button 1</Button>
                    <Button type="primary">Button 2</Button>
                </ButtonGroup>
            </div>

            <h2 id="icons">Icons</h2>
            <div className="aui-flatpack-example" id="icon-lists">
                <h5>Global icons</h5>
                {globalIcons.map(function (icon) {
                    return <Icon icon={icon} key={icon}/>;
                })}
                <h5>Dev tools icons</h5>
                {devToolsIcons.map(function (icon) {
                    return <Icon icon={icon} key={icon}/>;
                })}
                <h5>Editor icons</h5>
                {editorIcons.map(function (icon) {
                    return <Icon icon={icon} key={icon}/>;
                })}
                <h5>JIRA icons</h5>
                {jiraIcons.map(function (icon) {
                    return <Icon icon={icon} key={icon}/>;
                })}
            </div>

            <h2 id="lozenges">Lozenges</h2>
            <div className="aui-flatpack-example">
                <p>
                    <Lozenge>Default</Lozenge>
                    <Lozenge type="success">Success</Lozenge>
                    <Lozenge type="error">Error</Lozenge>
                    <Lozenge type="current">Current</Lozenge>
                    <Lozenge type="new">New</Lozenge>
                    <Lozenge type="moved">Moved</Lozenge>
                </p>
                <p>
                    <Lozenge subtle>Default</Lozenge>
                    <Lozenge subtle type="success">Success</Lozenge>
                    <Lozenge subtle type="error">Error</Lozenge>
                    <Lozenge subtle type="current">Current</Lozenge>
                    <Lozenge subtle type="new">New</Lozenge>
                    <Lozenge subtle type="moved">Moved</Lozenge>
                </p>
            </div>

            <h2 id="tables">Tables</h2>
            <Table>
                <thead>
                <tr>
                    <th>#</th>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>Username</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1</td>
                    <td>Matt</td>
                    <td>Bond</td>
                    <td>mbond</td>
                </tr>
                </tbody>
            </Table>

            <h2 id="tabs">Tabs</h2>
            <div className="aui-flatpack-example">
                <Tabs>
                    <Tab label="First Tab" id="first-tab">
                        <p>Content of the <strong>first</strong> tab</p>
                    </Tab>

                    <Tab label={(<Fragment>Second Tab <Badge text="123" /></Fragment>)} id="second-tab">
                        You can pass components as a tab label and to the tab content.
                        It simply <Lozenge type="success">works</Lozenge>
                    </Tab>

                    <Tab label="Last tab" id="last-tab" isActive>
                        <p>The last tab will be an active tab</p>
                    </Tab>
                </Tabs>
            </div>


            <h2 id="application-header">Application header</h2>
            <div className="aui-flatpack-example" style={{height: 40}}>
                <ApplicationHeader logo="aui"/>
            </div>
        </PageContent>
    </div>
), document.getElementById('root'));
