/* istanbul ignore next */
export function body() {
    return typeof document !== 'undefined' && document.body ? document.body : {};
}
