import React from 'react';
import PropTypes from 'prop-types';

/**
 * Badges provide quick visual identification for numeric values such as tallies and other quantities. They are not used
 * for anything other than integers. For statuses, use Lozenges. To call out tags or other high-visibility attributes,
 * use Labels.
 */
const AUIBadge = ({ id, text }) => (
    <span className="aui-badge" id={id}>{text}</span>
);

AUIBadge.propTypes = {
    /**
     * Visible text of the badge (usually a number).
     */
    text: PropTypes.string.isRequired,
    /**
     * ID attribute.
     */
    id: PropTypes.string
};

export default AUIBadge;
